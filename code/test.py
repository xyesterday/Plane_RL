import pandas as pd
import numpy as np
import random

#飞机的个数与随机数范围，随机数间隔
plane_num = 20
scope_min = 0
scope_max = 60
scope_step = 5

#随机飞机起飞时间序列，随机飞机的种类（小0，中1，大2）
random_plane_etd = []   # 飞机计划起飞时间
random_plane_size = []  # 飞机的大小
random_plane_etdp = []  # 飞机的计划离港时间
random_plane_airport = []   # 飞机的离港点
random_plane_outport = []   # 飞机的离港汇聚点

#随机生成
for i in range(plane_num):
    time = random.randrange(scope_min , scope_max , scope_step)
    random_plane_etd.append(time)
    random_plane_size.append(random.randint(0 , 2))
    random_plane_etdp.append(time + 10)
    random_plane_airport.append(chr(random.randint(65 , 66)))
    random_plane_outport.append(chr(random.randint(97 , 98)))

#随机起飞时间初始序列按照从小到大排列
random_plane_etd.sort(reverse = False)
random_plane_etdp.sort(reverse = False)

#生成dataframe，并写入到文件中
dataframe = pd.DataFrame({"etd" : random_plane_etd , "Size" : random_plane_size , "etdp" : random_plane_etdp , "airport" : random_plane_airport , "outport": random_plane_outport})
dataframe.to_csv(r"F:\plane_information\open.csv")
print("已写如文件中，注意粘贴")
